module.exports = {
  title: "番茄UI",
  description: "番茄UI 组件库 Vue 移动端 Ui 前端 Css JavaScript scss",
  head: [
    [ 
      "link",
      {
        rel: "icon",
        href: "http://www.tomatoui.cn/favicon.ico"
      }
    ],
  ],
  // base: './',
  markdown: {
    lineNumbers: false
  },
  themeConfig: {
    logo: "http://www.tomatoui.cn/logo.png",
    nav: [
      {
        text: "组件",
        link: "/guide/Cell"
      },
      {
        text: "全局方法",
        link: "/api/"
      },
      {
        text: "更新日志",
        link: "/log/"
      },
      {
        text: "Gitee",
        link: "https://gitee.com/woshizheqiqiu/tomato-ui",
        target: "_blank"
      }
    ],
    displayAllHeaders: true,
    // sidebar: {
    //   '/guide/': ['', 'Cell', 'Form'],
    //   '/api/': [''],
    //   '/log/': ['']
    // },
    sidebar: [
      {
        title: "组件",
        collapsable: true, //  collapsable: false为永远展开状态
        sidebarDepth: 1, // 可选的, 默认值是 1
        children: [
          {
            title:'快速开始',
            path:"guide/Quick"
          },
          {
            title:'ActionSheet',
            path:"guide/ActionSheet"
          },
          {
            title: "TabBar 底部导航",
            path: "/guide/TabBar"
          },
          {
            title: "Cell 单元格",
            path: "/guide/Cell"
          },
          {
            title: "Stepper 步进器",
            path: "/guide/Stepper"
          },
          {
            title: "Form 表单",
            path: "/guide/Form"
          },
          {
            title: "SearchBar 搜索栏",
            path: "/guide/SearchBar"
          },
          {
            title: "Sticky 吸顶",
            path: "/guide/Sticky"
          },
          {
            title: "Toast 轻提示",
            path: "/guide/Toast"
          },
          {
            title: "Loading 加载提示",
            path: "/guide/Loading"
          },
          {
            title: "BackTop 返回顶部",
            path: "/guide/BackTop"
          },
          {
            title: "Input 输入框",
            path: "/guide/Input"
          }
        ],
      },
      {
        title: "方法",
        path: "/api/",
        children: [""],
        collapsable: true,
        initialOpenGroupIndex: 1,
      },
      {
        title: "更新日志",
        path: "/log/",
        collapsable: true,
        initialOpenGroupIndex: 1,
      },
    ],
    sidebarDepth: 2,
    lastUpdated: "最近更新",
    smoothScroll: true
  }
}
