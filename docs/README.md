---
home: true
heroImage: http://www.tomatoui.cn/logo.png
heroText: 番茄UI组件库
tagline: 一款简单易用的移动端组件库
actionText: 芜湖~ 起飞 →
actionLink: /guide/
features:
- title: 简洁至上
  details: 不管是哪种开发岗位，都可以快速上手该组件库
- title: 丝滑体验
  details: 享受 Vue3.x + Vite2.x 带来的丝滑组件体验
- title: 文档
  details: 完善的文档帮助你更加快速、全面的了解和使用组件库
footer: MIT Licensed | tomatoUI
---