# 快速开始

### 安装

```bash
npm install tomato-design 
yarn add tomato-design
pnpm add tomato-design
```


### 全局引入

```js
import {createApp} from "vue";
import App from "./App.vue";

import "tomato-design/tomato-ui.css";
import TMDesign from "tomato-design";

const app = createApp(App);
app.use(TMDesign).mount("#app")
```
以上代码便完成了 tomato-design 的引入。需要注意的是，样式文件需要单独引入。

### 引入部分组件
```js
import {Button,ActionSheet} from "tomato-design/packages";

```


